import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "./App.css";
import BlogList from "./src/BlogList";
import BlogPage from "./src/BlogPage";
import CreateBlog from "./src/CreateBlog";
import { getAllPosts } from "./src/requests";

if (!getAllPosts()) {
  localStorage.setItem("posts", "[]");
}

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <BlogList />
        </Route>
        <Route exact path="/blog/:id">
          <BlogPage />
        </Route>
        <Route exact path="/add">
          <CreateBlog />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
