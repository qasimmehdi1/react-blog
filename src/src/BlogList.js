import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { getAllPosts } from "./requests";

export default function BlogList() {
  const [blogs, setBlogs] = useState([]);

  useEffect(() => {
    setBlogs(getAllPosts());
  }, []);

  return (
    <div>
      <h1>Blogs</h1>

      <ul>
        {blogs.map((b) => (
          <li key={b.id}>
            <Link to={`/blog/${b.id}`}>{b.title}</Link>
          </li>
        ))}
      </ul>
      <Link to="/add">Add New</Link>
    </div>
  );
}
