import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import { addComment, getPostByID } from "./requests";

export default function BlogPage() {
  const { id } = useParams();
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  const [comments, setComments] = useState([]);

  const [newComment, setNewComment] = useState("");
  const [refresh, setRefresh] = useState(true);

  useEffect(() => {
    const post = getPostByID(id);
    console.log(post);
    setTitle(post.title);
    setBody(post.body);
    setComments(post.comments);
  }, [id, refresh]);

  const saveComment = () => {
    addComment(id, newComment);
    setRefresh((s) => !s);
  };

  return (
    <div>
      <h1>Blog</h1>
      <p>{title}</p>
      <p>{body}</p>

      <h3>Comments</h3>
      <ul>
        {comments.map((c, i) => (
          <li key={`comment-${i}`}>{c}</li>
        ))}
      </ul>
      <textarea
        value={newComment}
        onChange={(e) => setNewComment(e.target.value)}
        placeholder="Add comment"
        rows="3"
      ></textarea>
      <br />
      <button disabled={!newComment} onClick={saveComment}>
        Save
      </button>
    </div>
  );
}
