import React, { useState } from "react";
import { useHistory } from "react-router";
import { addPost } from "./requests";

export default function CreateBlog() {
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  const history = useHistory();

  const handleSave = () => {
    const data = {
      id: Date.now(),
      title,
      body,
      comments: [],
    };
    addPost(data);
    history.push("/");
  };

  return (
    <div>
      <h1>Add Blog</h1>

      <input
        type="text"
        value={title}
        onChange={(e) => setTitle(e.target.value)}
        placeholder="Title"
      />
      <br />
      <textarea
        value={body}
        onChange={(e) => setBody(e.target.value)}
        placeholder="Body"
        rows="6"
      ></textarea>
      <br />
      <button onClick={handleSave}>Submit</button>
    </div>
  );
}
