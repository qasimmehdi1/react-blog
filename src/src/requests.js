/* eslint-disable eqeqeq */
const addPost = (data) => {
  const posts = getAllPosts();
  posts.push(data);
  localStorage.setItem("posts", JSON.stringify(posts));
};

const getPostByID = (id) => {
  const posts = getAllPosts();
  return posts.find((post) => post.id == id);
};

const getAllPosts = () => {
  return JSON.parse(localStorage.getItem("posts"));
};

const addComment = (id, comment) => {
  const posts = getAllPosts();
  posts.find((post) => post.id == id).comments.push(comment);
  localStorage.setItem("posts", JSON.stringify(posts));
};

export { addPost, getAllPosts, getPostByID, addComment };
